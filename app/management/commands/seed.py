from django.core.management.base import BaseCommand
from app.seeds import character, tag, scene


class Command(BaseCommand):
    help = 'Runs all seeders'

    def handle(self, *args, **options):
        tag.seed()
        character.seed()
        scene.seed()