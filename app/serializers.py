from rest_framework import serializers

from app.models import Character, TaggableTag, Tag, Scene


class TagSerializer(serializers.ModelSerializer):
    class Meta:
        model = Tag
        fields = ('id', 'name',)


class TaggableTagSerializer(serializers.ModelSerializer):
    tag = TagSerializer()

    class Meta:
        model = TaggableTag
        fields = ('tag',)


class CharacterSerializer(serializers.HyperlinkedModelSerializer):
    tags = TaggableTagSerializer(many=True, required=False, read_only=True)

    class Meta:
        model = Character
        fields = ('id', 'name', 'date_of_birth', 'description', 'tags')
        validators = []


class SceneSerializer(serializers.HyperlinkedModelSerializer):
    tags = TaggableTagSerializer(many=True, required=False, read_only=True)
    characters = CharacterSerializer(many=True, required=False)

    class Meta:
        model = Scene
        fields = ('id', 'name', 'chronology', 'body', 'characters', 'tags')
        validators = []
