from django_seed import Seed

from app.models import Tag


def seed():
    Tag.objects.all().delete()

    seeder = Seed.seeder()
    seeder.add_entity(Tag, 25, {
        'name': lambda x: seeder.faker.word(),
    })
    seeder.execute()
