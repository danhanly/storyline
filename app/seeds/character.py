from django.contrib.contenttypes.models import ContentType
from django_seed import Seed

from app.models import Character, Tag, TaggableTag


def seed():
    # Delete all existing entries
    Character.objects.all().delete()
    character_type = ContentType.objects.get(app_label="app", model="character")
    TaggableTag.objects.filter(taggable_type=character_type).delete()

    # Seed new data
    seeder = Seed.seeder()
    seeder.add_entity(Character, 5, {
        'name': lambda x: seeder.faker.name(),
        'date_of_birth': lambda x: seeder.faker.date_of_birth(),
        'description': lambda x: ''.join('<p>' + item + '</p>' for item in seeder.faker.paragraphs(nb=seeder.faker.random_int(min=4, max=10))),
    })
    seeder.execute()

    seeder.add_entity(TaggableTag, 10, {
        'tag': lambda x: Tag.objects.order_by('?').first(),
        'taggable_id': lambda x: Character.objects.order_by('?').first().id,
        'taggable_type': lambda x: ContentType.objects.get(app_label="app", model="character")
    })
    seeder.execute()
