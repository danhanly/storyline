from django.contrib.contenttypes.models import ContentType
from django_seed import Seed

from app.models import TaggableTag, Tag, Scene, Character


def seed():
    # Delete all existing entries
    Scene.objects.all().delete()
    scene_type = ContentType.objects.get(app_label="app", model="scene")
    TaggableTag.objects.filter(taggable_type=scene_type).delete()

    seeder = Seed.seeder()

    # Create some scenes
    seeder.add_entity(Scene, 5, {
        'name': lambda x: seeder.faker.sentence(),
        'chronology': lambda x: seeder.faker.random_number(digits=2),
        'body': lambda x: ''.join('<p>' + item + '</p>' for item in seeder.faker.paragraphs(nb=seeder.faker.random_int(min=10, max=35))),
    })
    seeder.execute()

    # Apply a random amount of characters to each scene
    for scene in Scene.objects.all():
        character_count = seeder.faker.random_int(min=1, max=4)
        characters = Character.objects.all()[:character_count]
        scene.characters.set(characters)

    # Tag the scenes
    seeder.add_entity(TaggableTag, 10, {
        'tag': lambda x: Tag.objects.order_by('?').first(),
        'taggable_id': lambda x: Scene.objects.order_by('?').first().id,
        'taggable_type': lambda x: ContentType.objects.get(app_label="app", model="scene")
    })
    seeder.execute()