from django.middleware.csrf import _get_new_csrf_string


class CSRFResponseMiddleware:

    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        response = self.get_response(request)
        response['X-CSRFToken'] = _get_new_csrf_string()
        return response
