from rest_framework import viewsets
from rest_framework.decorators import action
from rest_framework.response import Response

from app.models import Tag
from app.serializers import TagSerializer


class TagViewSet(viewsets.ModelViewSet):
    queryset = Tag.objects.all()
    serializer_class = TagSerializer

    @action(detail=False, methods=['get'])
    def search(self, request):
        if request.query_params.get('query'):
            tags = Tag.objects.filter(name__icontains=request.query_params.get('query'))
            page = self.paginate_queryset(tags)
            if page is not None:
                serializer = self.get_serializer(page, many=True)
                return self.get_paginated_response(serializer.data)

            serializer = self.get_serializer(tags, many=True)
            return Response(serializer.data)
        else:
            return Response()
