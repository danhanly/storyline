from django.contrib.contenttypes.models import ContentType
from rest_framework import viewsets
from rest_framework.decorators import action
from rest_framework.response import Response

from app.models import Character, Tag, TaggableTag
from app.serializers import CharacterSerializer


class CharacterViewSet(viewsets.ModelViewSet):
    queryset = Character.objects.all()
    serializer_class = CharacterSerializer

    @action(detail=False, methods=['get'])
    def search(self, request):
        if request.query_params.get('query'):
            characters = Character.objects.filter(name__icontains=request.query_params.get('query'))
            page = self.paginate_queryset(characters)
            if page is not None:
                serializer = self.get_serializer(page, many=True)
                return self.get_paginated_response(serializer.data)

            serializer = self.get_serializer(characters, many=True)
            return Response(serializer.data)
        else:
            return Response()

    @action(detail=True, methods=['post'])
    def tag(self, request, pk=None):
        tag = Tag.objects.get(pk=request.data.get('tag'))
        character = self.get_object()
        TaggableTag.objects.create(taggable=character, tag_id=tag.id)
        character = self.get_object()

        serializer = self.get_serializer(character)
        return Response(serializer.data)

    @action(detail=True, methods=['delete'])
    def untag(self, request, pk=None):
        tag = Tag.objects.get(pk=request.query_params.get('tag'))
        character = self.get_object()
        # Remove the association
        character_type = ContentType.objects.get(app_label="app", model="character")
        taggable = TaggableTag.objects.get(
            taggable_type=character_type,
            taggable_id=character.id,
            tag_id=tag.id
        )
        taggable.delete()

        character = self.get_object()
        serializer = self.get_serializer(character)
        return Response(serializer.data)
