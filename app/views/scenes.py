from django.contrib.contenttypes.models import ContentType
from rest_framework import viewsets
from rest_framework.decorators import action
from rest_framework.response import Response

from app.models import Scene, Tag, TaggableTag, Character
from app.serializers import SceneSerializer


class SceneViewSet(viewsets.ModelViewSet):
    queryset = Scene.objects.all()
    serializer_class = SceneSerializer

    def get_queryset(self):
        return Scene.objects.all().order_by('chronology')

    @action(detail=True, methods=['post','delete'])
    def character(self, request, pk=None):
        scene = self.get_object()

        if request.method == 'POST':
            character = Character.objects.get(pk=request.data.get('character'))
            scene.characters.add(character)

        if request.method == 'DELETE':
            character = Character.objects.get(pk=request.query_params.get('character'))
            scene.characters.remove(character)

        serializer = self.get_serializer(self.get_object())
        return Response(serializer.data)

    @action(detail=True, methods=['post'])
    def tag(self, request, pk=None):
        tag = Tag.objects.get(pk=request.data.get('tag'))
        scene = self.get_object()
        TaggableTag.objects.create(taggable=scene, tag_id=tag.id)
        character = self.get_object()

        serializer = self.get_serializer(character)
        return Response(serializer.data)

    @action(detail=True, methods=['delete'])
    def untag(self, request, pk=None):
        tag = Tag.objects.get(pk=request.query_params.get('tag'))
        scene = self.get_object()
        # Remove the association
        scene_type = ContentType.objects.get(app_label="app", model="scene")
        taggable = TaggableTag.objects.get(
            taggable_type=scene_type,
            taggable_id=scene.id,
            tag_id=tag.id
        )
        taggable.delete()

        scene = self.get_object()
        serializer = self.get_serializer(scene)
        return Response(serializer.data)
