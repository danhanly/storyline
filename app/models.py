from django.contrib.contenttypes.fields import GenericForeignKey, GenericRelation
from django.contrib.contenttypes.models import ContentType
from django.db import models


class Tag(models.Model):
    name = models.CharField(max_length=200)


class TaggableTag(models.Model):
    tag = models.ForeignKey(Tag, on_delete=models.CASCADE)
    taggable_id = models.PositiveIntegerField()
    taggable_type = models.ForeignKey(ContentType, on_delete=models.CASCADE)
    taggable = GenericForeignKey('taggable_type', 'taggable_id')

    class Meta:
        db_table = 'app_taggable_tags'
        unique_together = (('tag', 'taggable_id', 'taggable_type'),)


class Character(models.Model):
    name = models.CharField(max_length=200)
    date_of_birth = models.DateField(default=None, null=True)
    description = models.TextField(default=None, null=True)
    tags = GenericRelation(TaggableTag, content_type_field='taggable_type', object_id_field='taggable_id')


class Scene(models.Model):
    name = models.CharField(max_length=200)
    chronology = models.IntegerField(default=0, null=True)
    body = models.TextField(default=None, null=True)
    characters = models.ManyToManyField(Character)
    tags = GenericRelation(TaggableTag, content_type_field='taggable_type', object_id_field='taggable_id')
