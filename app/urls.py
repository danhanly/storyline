# Dependencies
from django.conf.urls import url
from django.urls import path, include
from django.views.decorators.csrf import ensure_csrf_cookie
from django.views.generic import TemplateView
from rest_framework import routers

from app.views.characters import CharacterViewSet
from app.views.scenes import SceneViewSet
from app.views.tags import TagViewSet

router = routers.DefaultRouter()
router.register(r'characters', CharacterViewSet)
router.register(r'tags', TagViewSet)
router.register(r'scenes', SceneViewSet)

urlpatterns = [
    # Application Data URLs
    path('data/', include(router.urls)),
    # Fallback for all Vue Router routes
    url(r'^((?!data\/).*$)', ensure_csrf_cookie(TemplateView.as_view(template_name="index.html"))),
]
