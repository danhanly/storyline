Vue.mixin({
    computed: {
        /**
         * Options for the TinyMCE WYSIWYG
         *
         * @return {Object}
         */
        tinyMceOptions() {
            return {
                statusbar: false,
                menubar: false,
                toolbar: 'undo redo | bold italic underline | link | fullscreen',
                height: 300,
            }
        },
        /**
         * Plugin options for the TinyMCE WYSIWYG
         *
         * @return {Array}
         */
        tinyMcePlugins() {
            return [
                'autolink link searchreplace fullscreen',
            ]
        },
    },
})