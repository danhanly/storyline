/**
 * WINDOW
 */
window.axios = require('axios');
window.axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
window.axios.defaults.xsrfCookieName = 'csrftoken';
window.axios.defaults.xsrfHeaderName = "X-CSRFTOKEN";

import {dom, library} from '@fortawesome/fontawesome-svg-core'
import {fas} from '@fortawesome/free-solid-svg-icons'
/**
 * VUE JS
 */
import Vue from 'vue'
import VueRouter from 'vue-router'
import router from './routes'
import Datetime from 'vue-datetime'
import {FontAwesomeIcon} from "@fortawesome/vue-fontawesome";
import moment from 'moment'
import _ from 'lodash'
import './directives'

window.Vue = Vue;

library.add(fas);
dom.watch();

Vue.prototype.moment = moment;
Vue.prototype._ = _;

// Dependencies
Vue.use(VueRouter);
Vue.use(Datetime);

// Global Components
Vue.component('navigation', require('./components/Navigation.vue').default);
Vue.component('add-tag', require('./components/AddTag.vue').default);
Vue.component('tag', require('./components/Tag.vue').default);
Vue.component('add-character', require('./components/AddCharacter.vue').default);
Vue.component('character', require('./components/Character.vue').default);
Vue.component('tinymce', require('vue-easy-tinymce'));
Vue.component('fa', FontAwesomeIcon.default);

// Application
const app = new Vue({
    el: '#app',
    router,
    mixins: [
        require('./mixins/TinyMce'),
    ],
});

/**
 * MISCELLANEOUS DEPENDENCIES
 */
require('../scss/styles.scss');