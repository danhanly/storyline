import VueRouter from 'vue-router'

const Dashboard = require('./components/views/Dashboard.vue').default;
const StorylineView = require('./components/views/Storyline/View.vue').default;
const CharacterList = require('./components/views/Character/List.vue').default;
const CharacterView = require('./components/views/Character/View.vue').default;
const CharacterCreate = require('./components/views/Character/Create.vue').default;
const SceneList = require('./components/views/Scene/List.vue').default;
const SceneView = require('./components/views/Scene/View.vue').default;
const SceneCreate = require('./components/views/Scene/Create.vue').default;

const routes = [
    {
        name: 'dashboard',
        path: '/',
        component: Dashboard,
    },
    {
        name: 'storyline',
        path: '/storyline',
        component: StorylineView,
    },
    {
        name: 'characters',
        path: '/character',
        component: CharacterList,
        children: [
            {
                name: 'character.view',
                path: ':id',
                component: CharacterView
            },
            {
                name: 'character.create',
                path: 'create',
                component: CharacterCreate,
            },
        ],
    },
    {
        name: 'scenes',
        path: '/scene',
        component: SceneList,
        children: [
            {
                name: 'scene.view',
                path: ':id',
                component: SceneView
            },
            {
                name: 'scene.create',
                path: 'create',
                component: SceneCreate,
            },
        ],
    }
];

export default new VueRouter({
    mode: 'history',
    base: '/',
    routes,
    scrollBehavior (to, from, savedPosition) {
        return { x: 0, y: 0 }
    }
})